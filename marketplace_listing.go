package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"

	"net/url"
)

// Create a Marketplace listing
func CreateListing(params []byte) (error, *models.ListingCreate) {
	data := new(models.ListingCreate)
	endpoint := "/marketplace/listings"
	err := Post(endpoint, params, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

// Get marketplace listing
func GetListingById(id int, params url.Values) (error, *models.Listing) {
	data := new(models.Listing)
	endpoint := fmt.Sprintf("/marketplace/listings/%d?", id)
	parameters := params.Encode()
	err := Get(endpoint+parameters, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

// Edit the data associated with a listing
func EditListingById(id int, params []byte) error {
	endpoint := fmt.Sprintf("/marketplace/listings/%d", id)
	err := Post(endpoint, params, nil)
	if err != nil {
		return err
	}

	return nil
}

// Permanently remove a listing from the Marketplace
func DeleteListingById(id int, params url.Values) error {
	endpoint := fmt.Sprintf("/marketplace/listings/%d?", id)
	parameters := params.Encode()
	err := Delete(endpoint + parameters)
	if err != nil {
		return err
	}

	return nil
}
