package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get an artist by ID.
func GetArtistById(id int) (error, *models.Artist) {
	data := new(models.Artist)
	endpoint := fmt.Sprintf("/artists/%d", id)

	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
