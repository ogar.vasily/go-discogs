package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get marketplace fee
func GetFee(price float64) (error, *models.MarketplaceFee) {
	data := new(models.MarketplaceFee)
	endpoint := fmt.Sprintf("/marketplace/fee/%f", price)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

// Get marketplace fee for particular currency
func GetFeeWithCurrency(price float64, currency string) (error, *models.MarketplaceFee) {
	data := new(models.MarketplaceFee)
	endpoint := fmt.Sprintf("/marketplace/fee/%f/%s", price, currency)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
