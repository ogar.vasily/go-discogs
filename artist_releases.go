package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get an artist's releases by artist ID
func GetArtistReleasesByArtistId(id int) (error, *models.ArtistReleases) {
	data := new(models.ArtistReleases)
	endpoint := fmt.Sprintf("/artists/%d/releases", id)
	err := Get(endpoint, data)

	if err != nil {
		return err, data
	}

	return nil, data
}
