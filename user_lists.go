package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get user lists
func GetUserListsByUsername(name string) (error, *models.UserLists) {
	data := new(models.UserLists)
	endpoint := fmt.Sprintf("/users/%s/lists", name)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
