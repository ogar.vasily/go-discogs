package limiter

import (
	"time"
	"sync"
)

var RateLimiter *Limiter

func init() {
	RateLimiter = NewLimiter(2, 2100*time.Millisecond)
}

type Limiter struct {
	mu    sync.Mutex
	limit []time.Time
	n     int
	d     time.Duration
}

func NewLimiter(max int, d time.Duration) *Limiter {
	return &Limiter{limit: make([]time.Time, max), d: d}
}

func (l *Limiter) Wait() {
	l.mu.Lock()
	defer l.mu.Unlock()

	i := l.n % len(l.limit)
	limit := l.limit[i]
	now := time.Now()

	if limit.After(now) {
		time.Sleep(limit.Sub(now))
		now = time.Now()
	}

	l.limit[i] = now.Add(l.d)
	l.n++
}
