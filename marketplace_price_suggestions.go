package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get price suggestions for the provided Release ID
func GetPriceSuggestionsByReleaseId(id int) (error, *models.PriceSuggestions) {
	data := new(models.PriceSuggestions)
	endpoint := fmt.Sprintf("/marketplace/price_suggestions/%d", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
