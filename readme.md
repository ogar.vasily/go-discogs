# API calls

## Database
- [x] Release (need to add support for optional parameter cur_abbr)
- [ ] Release rating by user
- [ ] Community release rating
- [x] Master release
- [x] Master release versions (need to add support for optional parameters page, per_page)
- [x] Artist
- [x] Artist releases (need to add support for optional parameters sort, sort_order, page, per_page)
- [x] Label
- [x] All label releases (need to add support for optional parameters page, per_page)
- [x] Search (need to add support for optional parameters page, per_page)

## Marketplace
- [x] Inventory (need to add support for optional parameters status, sort, sort_order, page, per_page)
- [x] Listing
- [x] New listing
- [x] Order
- [x] List orders
- [x] List order messages
- [x] Fee
- [x] Fee with currency
- [x] Price suggestions

## User identity
- [x] Identity
- [ ] Profile
- [ ] User submissions
- [ ] User contributions

## User collections
- [ ] Collection
- [ ] Collection folder
- [ ] Collection items by release
- [ ] Collection items by folder
- [ ] Add to collection folder
- [ ] Change rating of release
- [ ] Delete instance from folder
- [x] List custom fields
- [ ] Edit fields instance
- [ ] Collection value

## User wantlist
- [ ] Wantlist
- [ ] Add to wantlist

## User lists
- [x] User lists
- [x] Lists

