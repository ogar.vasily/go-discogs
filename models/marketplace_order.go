package models

import (
	"time"
)

type Order struct {
	ID          string    `json:"id"`
	ResourceURL string    `json:"resource_url", omitempty`
	MessagesURL string    `json:"messages_url"`
	URI         string    `json:"uri"`
	Status      string    `json:"status"`
	NextStatus  []string  `json:"next_status"`
	Fee         BasePrice `json:"fee"`
	Created     time.Time `json:"created"`
	Items       []struct {
		Release struct {
			ID          int    `json:"id"`
			Description string `json:"description"`
			ResourceURL string `json:"resource_url", omitempty`
			Thumbnail   string `json:"thumbnail", omitempty`
		} `json:"release"`
		Price           BasePrice `json:"price"`
		MediaCondition  string    `json:"media_condition", omitempty`
		SleeveCondition string    `json:"sleeve_condition", omitempty`
		ID              int       `json:"id"`
		ConditionComments string `json:"condition_comments"`
	} `json:"items"`
	Shipping               BaseShipping    `json:"shipping"`
	ShippingAddress        string          `json:"shipping_address"`
	AdditionalInstructions string          `json:"additional_instructions"`
	Seller                 BaseBuyerSeller `json:"seller"`
	LastActivity           time.Time       `json:"last_activity"`
	Buyer                  BaseBuyerSeller `json:"buyer"`
	Total                  BasePrice       `json:"total"`
}

type OrderEditResponse struct {
	ID          string    `json:"id"`
	ResourceURL string    `json:"resource_url"`
	MessagesURL string    `json:"messages_url"`
	URI         string    `json:"uri"`
	Status      string    `json:"status"`
	NextStatus  []string  `json:"next_status"`
	Fee         BasePrice `json:"fee"`
	Created     time.Time `json:"created"`
	Items       []struct {
		Release struct {
			ID          int    `json:"id"`
			Description string `json:"description"`
		} `json:"release"`
		Price BasePrice `json:"price"`
		ID    int       `json:"id"`
	} `json:"items"`
	Shipping               BaseShipping    `json:"shipping"`
	ShippingAddress        string          `json:"shipping_address"`
	AdditionalInstructions string          `json:"additional_instructions"`
	Seller                 BaseBuyerSeller `json:"seller"`
	LastActivity           time.Time       `json:"last_activity"`
	Buyer                  BaseBuyerSeller `json:"buyer"`
	Total                  BasePrice       `json:"total"`
}

type OrderList struct {
	Pagination BasePagination `json:"pagination"`
	Orders []Order `json:"orders"`
}

