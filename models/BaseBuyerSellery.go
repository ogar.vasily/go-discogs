package models

type BaseBuyerSeller struct {
	ResourceURL string `json:"resource_url"`
	Username    string `json:"username"`
	ID          int    `json:"id"`
	Email		string `json:"email",omitempty`
}
