package models

// Releases and Masters associated with the Artist.
type ArtistReleases struct {
	Pagination BasePagination  `json:"pagination"`
	Releases   []ArtistRelease `json:"releases"`
}

// Artist release
type ArtistRelease struct {
	Artist      string `json:"artist"`
	ID          int    `json:"id"`
	MainRelease int    `json:"main_release"`
	ResourceURL string `json:"resource_url"`
	Role        string `json:"role"`
	Thumb       string `json:"thumb"`
	Title       string `json:"title"`
	Type        string `json:"type"`
	Year        int    `json:"year"`
}
