package models

// List of all Releases that are versions of this master. Accepts Pagination parameters.
type MasterVersions struct {
	Pagination BasePagination `json:"pagination"`
	Versions   []struct {
		CatNo        string   `json:"catno"`
		Country      string   `json:"country"`
		Format       string   `json:"format"`
		ID           int      `json:"id"`
		Label        string   `json:"label"`
		Released     string   `json:"released"`
		ResourceURL  string   `json:"resource_url"`
		Status       string   `json:"status"`
		MajorFormats []string `json:"major_formats"`
		Thumb        string   `json:"thumb"`
		Title        string   `json:"title"`
	} `json:"versions"`
}
