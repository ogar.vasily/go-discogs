package models

type BaseImage struct {
	Height      int    `json:"height"`
	ResourceURL string `json:"resource_url"`
	Type        string `json:"type"`
	URI         string `json:"uri"`
	URI150      string `json:"uri_150"`
	Width       int    `json:"width"`
}
