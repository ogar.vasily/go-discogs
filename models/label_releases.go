package models

// Returns a list of Releases associated with the label.
type LabelReleases struct {
	Pagination BasePagination `json:"pagination"`
	Releases   []LabelRelease `json:"releases"`
}

// Release of label
type LabelRelease struct {
	Artist      string `json:"artist"`
	Catno       string `json:"catno"`
	Format      string `json:"format"`
	ID          int    `json:"id"`
	ResourceURL string `json:"resource_url"`
	Status      string `json:"status"`
	Thumb       string `json:"thumb"`
	Title       string `json:"title"`
	Year        int    `json:"year"`
}
