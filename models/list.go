package models

import (
	"time"
)

// Items from a specified List
type List struct {
	CreatedTs  time.Time `json:"created_ts"`
	ModifiedTs time.Time `json:"modified_ts"`
	Name       string    `json:"name"`
	ListID     int       `json:"list_id"`
	URL        string    `json:"url"`
	Items      []struct {
		Comment      string `json:"comment"`
		DisplayTitle string `json:"display_title"`
		URI          string `json:"uri"`
		ImageURL     string `json:"image_url"`
		ResourceURL  string `json:"resource_url"`
		Type         string `json:"type"`
		ID           int    `json:"id"`
	} `json:"items"`
	ResourceURL string `json:"resource_url"`
	Public      bool   `json:"public"`
	Description string `json:"description"`
}
