package models

//The Label resource represents a label, company, recording studio, location, or other entity involved with Artists and Releases.
type Label struct {
	Profile     string `json:"profile"`
	ReleasesURL string `json:"releases_url"`
	Name        string `json:"name"`
	ContactInfo string `json:"contact_info"`
	URI         string `json:"uri"`
	Sublabels   []struct {
		ResourceURL string `json:"resource_url"`
		ID          int    `json:"id"`
		Name        string `json:"name"`
	} `json:"sublabels"`
	URLs        []string    `json:"urls"`
	Images      []BaseImage `json:"images"`
	ResourceURL string      `json:"resource_url"`
	ID          int         `json:"id"`
	DataQuality string      `json:"data_quality"`
}
