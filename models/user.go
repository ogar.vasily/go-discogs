package models

import (
	"time"
)

type User struct {
	Profile string `json:"profile"`
    BannerUrl string `json:"banner_url"`
	WantlistUrl string `json:"wantlist_url"`
    SellerNumRatings int `json:"seller_num_ratings"`
    Rank int `json:"rank"`
    NumPending int `json:"num_pending"`
    ID int `json:id"`
    MarketplaceSuspended bool `json:"marketplace_suspended"`
    BuyerRating int `json:"buyer_rating"`
    NumUnread int `json:"num_unread"`
    NumForSale int `json:"num_for_sale"`
    HomePage string `json:"home_page"`
    Location string `json:"location"`
    CollectionFoldersUrl string `json:"collection_folders_url"`
    Email string `json:"email"`
    Username string `json:"username"`
    CollectionFieldsUrl string `json:"collection_fields_url"`
    ReleasesContributed int `json:"releases_contributed"`
    Activated bool `json:"activated"`
    Registered time.Time `json:"registered"`
    RatingAvg float64 `json:"rating_avg"`
    NumCollection int `json:"num_collection"`
    ReleasesRated int `json:"releases_rated"`
    CurrAbbr string `json:"curr_abbr"`
    SellerRatingStars float64 `json:"seller_rating_stars"`
    NumLists int `json:"num_lists"`
    Name string `json:"name"`
    BuyerRatingStars int `json:"buyer_rating_stars"`
    NumWantlist int `json:"num_wantlist"`
    InventoryUrl string `json:"inventory_url"`
    URI string `json:"uri"`
    BuyerNumRatings int `json:"buyer_num_ratings"`
    AvatarUrl string `json:"avatar_url"`
    ResourceUrl string `json:"resource_url"`
    SellerRating float64 `json:"seller_rating"`
}
