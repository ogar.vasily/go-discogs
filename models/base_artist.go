package models

// Base artist struct
type BaseArtist struct {
	Join        string `json:"join"`
	Name        string `json:"name"`
	ANV         string `json:"anv"`
	Tracks      string `json:"tracks"`
	Role        string `json:"role"`
	ResourceURL string `json:"resource_url"`
	ID          int    `json:"id"`
}
