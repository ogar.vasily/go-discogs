package models

import "github.com/shopspring/decimal"

// Base original price
type BaseOriginalPrice struct {
	CurrAbbr  string          `json:"curr_abbr"`
	CurrID    int             `json:"curr_id"`
	Formatted string          `json:"formatted"`
	Value     decimal.Decimal `json:"value"`
}
