package models

import "github.com/shopspring/decimal"

// The Release resource represents a particular physical or digital object released by one or more Artists.
type Release struct {
	Title       string       `json:"title"`
	ID          int          `json:"id"`
	Artists     []BaseArtist `json:"artists"`
	DataQuality string       `json:"data_quality"`
	Thumb       string       `json:"thumb"`
	Community   struct {
		Contributors []struct {
			ResourceURL string `json:"resource_url"`
			Username    string `json:"username"`
		} `json:"contributors"`
		DataQuality string `json:"data_quality"`
		Have        int    `json:"have"`
		Rating      struct {
			Average decimal.Decimal `json:"average"`
			Count   int             `json:"count"`
		} `json:"rating"`
		Status    string `json:"status"`
		Submitter struct {
			ResourceURL string `json:"resource_url"`
			Username    string `json:"username"`
		} `json:"submitter"`
		Want int `json:"want"`
	} `json:"community"`
	Companies []struct {
		CatNo          string `json:"catno"`
		EntityType     string `json:"entity_type"`
		EntityTypeName string `json:"entity_type_name"`
		ID             int    `json:"id"`
		Name           string `json:"name"`
		ResourceURL    string `json:"resource_url"`
	} `json:"companies"`
	Country         string       `json:"country"`
	DateAdded       string       `json:"date_added"`
	DateChanged     string       `json:"date_changed"`
	EstimatedWeight int          `json:"estimated_weight"`
	ExtraArtists    []BaseArtist `json:"extra_artists"`
	FormatQuantity  int          `json:"format_quantity"`
	Formats         []struct {
		Descriptions []string `json:"descriptions"`
		Name         string   `json:"name"`
		Qty          string   `json:"qty"`
	} `json:"formats"`
	Genres      []string `json:"genres"`
	Identifiers []struct {
		Type  string `json:"type"`
		Value string `json:"value"`
	} `json:"identifiers"`
	Images []BaseImage `json:"images"`
	Labels []struct {
		CatNo       string `json:"catno"`
		Entity_Type string `json:"entity_type"`
		ID          int    `json:"id"`
		Name        string `json:"name"`
		ResourceURL string `json:"resource_url"`
	} `json:"labels"`
	LowestPrice       decimal.Decimal `json:"lowest_price"`
	MasterID          int             `json:"master_id"`
	MasterURL         string          `json:"master_url"`
	Notes             string          `json:"notes"`
	Released          string          `json:"released"`
	ReleasedFormatted string          `json:"released_formatted"`
	ResourceURL       string          `json:"resource_url"`
	Series            []struct {
		ID          int    `json:"id"`
		ResourceURL string `json:"resource_url"`
		CatNo       string `json:"catno"`
		Name        string `json:"name"`
		EntityType  string `json:"entity_type"`
	} `json:"series"`
	Status    string   `json:"status"`
	Styles    []string `json:"styles"`
	Tracklist []struct {
		Duration string `json:"duration"`
		Position string `json:"position"`
		Title    string `json:"title"`
		Type_    string `json:"Type_"`
	} `json:"tracklist"`
	URI    string      `json:"uri"`
	Videos []BaseVideo `json:"videos"`
	Year   int         `json:"year"`
}
