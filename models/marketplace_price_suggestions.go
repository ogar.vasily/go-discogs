package models

// Marketplace Price suggestion response
type PriceSuggestions struct {
	VeryGood     BasePrice `json:"Very Good (VG)"`
	GoodPlus     BasePrice `json:"Good Plus (G+)"`
	NearMint     BasePrice `json:"Near Mint (NM or M-)"`
	Good         BasePrice `json:"Good (G)"`
	VeryGoodPlus BasePrice `json:"Very Good Plus (VG+)"`
	Mint         BasePrice `json:"Mint (M)"`
	Fair         BasePrice `json:"Fair (F)"`
	Poor         BasePrice `json:"Poor (P)"`
}
