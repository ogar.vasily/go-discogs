package models

import "github.com/shopspring/decimal"

// The Master resource represents a set of similar Releases
type Master struct {
	Styles         []string     `json:"styles"`
	Genres         []string     `json:"genres"`
	Videos         []BaseVideo  `json:"videos"`
	Title          string       `json:"title"`
	MainRelease    int          `json:"main_release"`
	MainReleaseURL string       `json:"main_release_url"`
	URI            string       `json:"uri"`
	Artists        []BaseArtist `json:"artists"`
	VersionsURL    string       `json:"versions_url"`
	Year           int          `json:"year"`
	Images         []BaseImage  `json:"images"`
	ResourceURL    string       `json:"resource_url"`
	Tracklist      []struct {
		Duration    string       `json:"duration"`
		Position    string       `json:"position"`
		Type        string       `json:"type_"`
		ExtraArtist []BaseArtist `json:"extra_artist"`
		Title       string       `json:"title"`
	} `json:"tracklist"`
	ID          int             `json:"id"`
	NumForSale  int             `json:"num_for_sale"`
	LowestPrice decimal.Decimal `json:"lowest_price"`
	DataQuality string          `json:"data_quality"`
}
