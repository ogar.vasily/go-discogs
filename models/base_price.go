package models


// Base price
type BasePrice struct {
	Currency string          `json:"currency"`
	Value    float64 `json:"value"`
}
