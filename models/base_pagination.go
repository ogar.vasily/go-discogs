package models

type BasePagination struct {
	PerPage int `json:"per_page"`
	Items   int `json:"items"`
	Page    int `json:"page"`
	URLs    struct {
		Last string `json:"last"`
		Next string `json:"next"`
	} `json:"urls"`
	Pages int `json:"pages"`
}
