package models


type BaseShipping struct {
	Currency string          `json:"currency"`
	Method   string          `json:"method"`
	Value    float64 `json:"value"`
}
