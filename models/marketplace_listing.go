package models

import (
	"github.com/shopspring/decimal"
	"time"
)

// Marketplace Listing response struct
type Listing struct {
	Status          string            `json:"status"`
	Price           BasePrice         `json:"price"`
	OriginalPrice   BaseOriginalPrice `json:"original_price"`
	AllowOffers     bool              `json:"allow_offers"`
	SleeveCondition string            `json:"sleeve_condition"`
	ID              int               `json:"id"`
	Condition       string            `json:"condition"`
	Posted          time.Time         `json:"posted"`
	ShipsFrom       string            `json:"ships_from"`
	URI             string            `json:"uri"`
	Comments        string            `json:"comments"`
	Seller          struct {
		Username    string `json:"username"`
		AvatarURL   string `json:"avatar_url"`
		ResourceURL string `json:"resource_url"`
		URL         string `json:"url"`
		ID          int    `json:"id"`
		Shipping    string `json:"shipping"`
		Payment     string `json:"payment"`
		Stats       struct {
			Rating string          `json:"rating"`
			Stars  decimal.Decimal `json:"stars"`
			Total  int             `json:"total"`
		} `json:"stats"`
	} `json:"seller"`
	ShippingPrice         BasePrice         `json:"shipping_price"`
	OriginalShippingPrice BaseOriginalPrice `json:"original_shipping_price"`
	Release               struct {
		CatalogNumber string `json:"catalog_number"`
		ResourceURL   string `json:"resource_url"`
		Year          int    `json:"year"`
		ID            int    `json:"id"`
		Description   string `json:"description"`
		Thumbnail     string `json:"thumbnail"`
	} `json:"release"`
	ResourceURL string `json:"resource_url"`
	Audio       bool   `json:"audio"`
	ExternalID      string      `json:"external_id,omitempty"`
	Weight          int             `json:"weight,omitempty"`
	FormatQuantity  int             `json:"format_quantity,omitempty"`
	Location        string             `json:"location,omitempty"`
}

// Marketplace Update listing request struct
type ListingEdit struct {
	ListingID       int             `json:"listing_id"`
	ReleaseID       int             `json:"release_id"`
	Condition       string          `json:"condition"`
	SleeveCondition string          `json:"sleeve_condition,omitempty"`
	Price           decimal.Decimal `json:"price"`
	Comments        string          `json:"comments,omitempty"`
	AllowOffers     bool            `json:"allow_offers,omitempty"`
	Status          string          `json:"status"`
	ExternalID      string             `json:"external_id,omitempty"`
	Location        string             `json:"location,omitempty"`
	Weight          int             `json:"weight,omitempty"`
	FormatQuantity  int             `json:"format_quantity,omitempty"`
}

// Marketplace New listing request struct
type ListingNew struct {
	ReleaseID       int             `json:"release_id"`
	Condition       string          `json:"condition"`
	SleeveCondition string          `json:"sleeve_condition,omitempty"`
	Price           decimal.Decimal `json:"price"`
	Comments        string          `json:"comments,omitempty"`
	AllowOffers     bool            `json:"allow_offers,omitempty"`
	Status          string          `json:"status"`
	ExternalID      string             `json:"external_id,omitempty"`
	Location        string             `json:"location,omitempty"`
	Weight          int             `json:"weight,omitempty"`
	FormatQuantity  int             `json:"format_quantity,omitempty"`
}

// Marketplace listing create response
type ListingCreate struct {
	ListingID   int    `json:"listing_id"`
	ResourceURL string `json:"resource_url"`
}
