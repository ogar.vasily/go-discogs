package models

import "time"

// User’s Lists
type UserLists struct {
	Pagination BasePagination `json:"pagination"`
	Lists      []struct {
		DateAdded   time.Time `json:"date_added"`
		DateChanged time.Time `json:"date_changed"`
		Name        string    `json:"name"`
		ID          int       `json:"id"`
		URI         string    `json:"uri"`
		ResourceURL string    `json:"resource_url"`
		Public      bool      `json:"public"`
		Description string    `json:"description"`
	}
}
