package models

// A list of user-defined collection notes fields
type UserCollectionCustomFields struct {
	Fields []struct {
		Name     string   `json:"name"`
		Options  []string `json:"options"`
		ID       int      `json:"id"`
		Position int      `json:"position"`
		Type     string   `json:"type"`
		Public   bool     `json:"public"`
	} `json:"fields"`
}
