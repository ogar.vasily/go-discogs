package models

// Search response
type Search struct {
	Pagination BasePagination `json:"pagination"`
	Results    []struct {
		Style     []string `json:"style"`
		Thumb     string   `json:"thumb"`
		Title     string   `json:"title"`
		Country   string   `json:"country"`
		Format    []string `json:"format"`
		URI       string   `json:"uri"`
		Community struct {
			Want int `json:"want"`
			Have int `json:"have"`
		} `json:"community"`
		Label       []string `json:"label"`
		CatNo       string   `json:"catno"`
		Year        string   `json:"year"`
		Genre       []string `json:"genre"`
		ResourceURL string   `json:"resource_url"`
		Type        string   `json:"type"`
		ID          int      `json:"id"`
	} `json:"results"`
}

// Search params
type SearchParams struct {
	Query        string
	Type         string
	Title        string
	ReleaseTitle string
	Credit       string
	Artist       string
	ANV          string
	Label        string
	Genre        string
	Style        string
	Country      string
	Year         string
	Format       string
	CatNo        string
	Barcode      string
	Track        string
	Submitter    string
	Contributor  string
}
