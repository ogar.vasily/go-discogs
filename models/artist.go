package models

// The Artist resource represents a person in the Discogs database who contributed to a Release in some capacity.
type Artist struct {
	Name           string   `json:"name"`
	NameVariations []string `json:"namevariations"`
	RealName       string   `json:"realname"`
	Aliases        []struct {
		ID          int    `json:"id"`
		Name        string `json:"name"`
		ResourceURL string `json:"resource_url"`
	} `json:"aliases"`
	Profile     string      `json:"profile"`
	ReleasesURL string      `json:"release_url"`
	ResourceURL string      `json:"resource_url"`
	URI         string      `json:"uri"`
	URLs        []string    `json:"urls"`
	DataQuality string      `json:"data_quality"`
	ID          int         `json:"id"`
	Images      []BaseImage `json:"images"`
	Members     []struct {
		Active      bool   `json:"active"`
		ID          int    `json:"id"`
		Name        string `json:"name"`
		ResourceURL string `json:"resource_url"`
	} `json:"members"`
}
