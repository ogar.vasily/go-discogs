package models

import (
	"time"
	"github.com/shopspring/decimal"
)


type Message struct {
		StatusID int `json:"status_id"`
		From     struct {
			ID          int    `json:"id"`
			Username    string `json:"username"`
			ResourceUrl string `json:"resource_url"`
			AvatarURL   string `json:"avatar_url"`
		} `json:"from"`
		Refund struct {
			Amount decimal.Decimal `json:"amount"`
			Order  struct {
				ResourceURL string `json:"resource_url"`
				ID          string `json:"id"`
			} `json:"order"`
		} `json:"refund"`
		Timestamp time.Time `json:"timestamp"`
		Actor     struct {
			Username    string `json:"username"`
			ResourceURL string `json:"resource_url"`
		} `json:"actor"`
		Original int    `json:"original"`
		New      int    `json:"new"`
		Message  string `json:"message"`
		Type     string `json:"type"`
		Order    struct {
			ResourceURL string `json:"resource_url"`
			ID          string `json:"id"`
		} `json:"order"`
		Subject string `json:"subject"`
}

type OrderMessages struct {
	Pagination BasePagination `json:"pagination"`
	Messages   []Message `json:"messages"`
}

type OrderMessagesEdit struct {
	Status    string `json:"status,omitempty"`
	Message   string `json:"message,omitempty"`
	ReleaseID int    `json:"release_id"`
}

type OrderMessagesEditResponse struct {
	From struct {
		Username    string `json:"username"`
		ResourceUrl string `json:"resource_url"`
	} `json:"from"`
	Message string `json:"message,omitempty"`
	Order   struct {
		ResourceURL string `json:"resource_url"`
		ID          string `json:"id"`
	} `json:"order"`
	Timestamp time.Time `json:"timestamp"`
	Subject   string `json:"subject"`
}
