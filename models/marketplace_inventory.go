package models

import (
	"time"
)

// The list of listings in a user’s inventory
type Inventory struct {
	Pagination BasePagination `json:"pagination"`
	Listings   []struct {
		Status          string    `json:"status"`
		Price           BasePrice `json:"price"`
		AllowOffers     bool      `json:"allow_offers"`
		SleeveCondition string    `json:"sleeve_condition"`
		ID              int       `json:"id"`
		Condition       string    `json:"condition"`
		Posted          time.Time `json:"posted"`
		ShipsFrom       string    `json:"ships_from"`
		URI             string    `json:"uri"`
		Comments        string    `json:"comments"`
		Seller          []struct {
			Username    string `json:"username"`
			ResourceURL string `json:"resource_url"`
			ID          int    `json:"id"`
		} `json:"seller"`
		Release struct {
			CatalogNumber string `json:"catalog_number"`
			ResourceURL   string `json:"resource_url"`
			Year          int    `json:"year"`
			ID            int    `json:"id"`
			Description   string `json:"description"`
			Artist        string `json:"artist"`
			Title         string `json:"title"`
			Format        string `json:"format"`
			Thumbnail     string `json:"thumbnail"`
		} `json:"release"`
		ResourceURL string `json:"resource_url"`
		Audio       bool   `json:"audio"`
	} `json:"listings"`
}
