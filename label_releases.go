package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get a label's releases by ID
func GetLabelReleasesById(id int) (error, *models.LabelReleases) {
	data := new(models.LabelReleases)
	endpoint := fmt.Sprintf("/labels/%d/releases", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
