package tests

import (
	"testing"
	discogs "gitlab.com/ogar.vasily/go-discogs"
)

func TestGetReleaseById(t *testing.T) {
	err, release := discogs.GetReleaseById(249504)

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}

	if release.ID != 249504 {
		t.Errorf("Wrong release ID")
	}
}
