package tests

import (
	"testing"
	discogs "gitlab.com/ogar.vasily/go-discogs"
)

func TestGetListById(t *testing.T) {
	// id of the list "Most expensive items sold in Discogs Marketplace for December 2017"
	err, list := discogs.GetListById(390968)

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}

	if len(list.Items) == 0 {
		t.Errorf("Cant get list")
	}
}
