package tests

import (
	"testing"
	discogs "gitlab.com/ogar.vasily/go-discogs"
)

// Get an artist by ID.
func TestGetArtistById(t *testing.T) {
	err, artist := discogs.GetArtistById(108713)

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}

	if artist.ID != 108713 {
		t.Errorf("Wrong artist ID")
	}
}
