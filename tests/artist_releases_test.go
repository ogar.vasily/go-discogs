package tests

import (
	"testing"
	discogs "gitlab.com/ogar.vasily/go-discogs"
)

func TestGetArtistReleasesByArtistId(t *testing.T) {
	err, release := discogs.GetArtistReleasesByArtistId(108713)

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}

	if release.Releases[0].Artist != "Nickelback" {
		t.Errorf("Wrong artist")
	}
}
