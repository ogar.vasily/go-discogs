package tests

import (
	"testing"
	discogs "gitlab.com/ogar.vasily/go-discogs"
)

func TestGetLabelById(t *testing.T) {
	err, label := discogs.GetLabelById(1)

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}

	if label.ID != 1 {
		t.Errorf("Cant get label")
	}
}
