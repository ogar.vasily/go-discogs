package tests

import (
	"testing"
	discogs "gitlab.com/ogar.vasily/go-discogs"
)

func TestGetIdentity(t *testing.T) {
	err, identity := discogs.GetIdentity()

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}

	if identity.Username == "" {
		t.Errorf("Cant get identity")
	}
}
