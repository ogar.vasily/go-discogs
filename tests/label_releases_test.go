package tests

import (
	"testing"
	discogs "gitlab.com/ogar.vasily/go-discogs"
)

func TestGetLabelReleasesById(t *testing.T) {
	err, label := discogs.GetLabelReleasesById(1)

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}

	if label.Releases[0].ID != 2801 {
		t.Errorf("Cant get label releases")
	}
}
