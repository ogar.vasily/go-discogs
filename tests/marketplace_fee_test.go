package tests

import (
	"testing"
	discogs "gitlab.com/ogar.vasily/go-discogs"
	"github.com/shopspring/decimal"
)

func TestGetFee(t *testing.T) {
	err, fee := discogs.GetFee(10)
	zero := decimal.Zero

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}

	if fee.Value.Equal(zero) {
		t.Errorf("Cant get fee")
	}
}

func TestGetFeeWithCurrency(t *testing.T) {
	err, fee := discogs.GetFeeWithCurrency(10, "USD")

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	zero := decimal.Zero

	if fee.Currency != "USD" {
		t.Errorf("Fee currency not equal USD")
	}

	if fee.Value.Equal(zero) {
		t.Errorf("Cant get fee")
	}
}
