package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get a master by ID
func GetMasterReleaseById(id int) (error, *models.Master) {
	data := new(models.Master)
	endpoint := fmt.Sprintf("/masters/%d", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
