package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get a list of user-defined collection notes fields
func GetUserCollectionCustomFieldsByUsername(name string) (error, *models.UserCollectionCustomFields) {
	data := new(models.UserCollectionCustomFields)
	endpoint := fmt.Sprintf("/users/%s/collection/fields", name)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
