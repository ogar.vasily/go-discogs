package go_discogs

import (
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get a label's releases by ID.
func GetIdentity() (error, *models.Identity) {
	data := new(models.Identity)
	endpoint := "/oauth/identity"
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
