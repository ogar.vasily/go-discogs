package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)


func GetUser(name string) (error, *models.User) {
	data := new(models.User)
	endpoint := fmt.Sprintf("/users/%s", name)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
