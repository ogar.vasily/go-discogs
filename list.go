package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get list's items from a specified List
func GetListById(id int) (error, *models.List) {
	data := new(models.List)
	endpoint := fmt.Sprintf("/lists/%d", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
