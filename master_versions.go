package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get a version of a master by ID
func GetMasterVersionsById(id int) (error, *models.MasterVersions) {
	data := new(models.MasterVersions)
	endpoint := fmt.Sprintf("/masters/%d/versions", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
