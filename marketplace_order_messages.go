package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
	"net/url"
)

// Adds a new message to the order’s message log
func CreateOrderMessageByOrderId(id string, params []byte) (error, *models.OrderMessagesEditResponse) {
	data := new(models.OrderMessagesEditResponse)
	endpoint := fmt.Sprintf("/marketplace/orders/%s/messages", id)
	err := Post(endpoint, params, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

// Get list of the order’s messages with the most recent first
func GetOrderMessageByOrderId(id, page, per_page string) (error, *models.OrderMessages) {
	data := new(models.OrderMessages)

	params := url.Values{}
	if page != "" {
		params.Add("page", page)
	}

	if per_page != "" {
		params.Add("per_page", per_page)
	}

	parameters := params.Encode()

	endpoint := fmt.Sprintf("/marketplace/orders/%s/messages?", id)
	endpoint = endpoint + parameters
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
