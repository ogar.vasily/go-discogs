package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get a release by ID
func GetReleaseById(id int) (error, *models.Release) {
	data := new(models.Release)
	endpoint := fmt.Sprintf("/releases/%d", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
