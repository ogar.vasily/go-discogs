package go_discogs

import (
	"path/filepath"
	"io/ioutil"
	"encoding/json"
	"fmt"
)

var token = new(Token)
var credPath string
var HeaderToken string

type Token struct {
	Token string `json:"token"`
}


func main() {
}


func authorizationHeaderToken(token string) string {
	base := "token=" + fmt.Sprintf("%s", token)
	return "Discogs " + base
}


func SetCredentials(filepath string) {
	credPath = filepath
	getCredentials()
}

func getCredentials() {
	if credPath == "" {
		panic("Please set Credentials file path")
	}
	file, _ := filepath.Abs(credPath)
	b, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(b, &token)

	base := "token=" + fmt.Sprintf("%s", token.Token)
	HeaderToken = "Discogs " + base
}
