package go_discogs

import (
	"gitlab.com/ogar.vasily/go-discogs/models"

	"net/url"
)

// Make a search
func Search(params url.Values) (error, *models.Search) {
	data := new(models.Search)
	parameters := params.Encode()
	endpoint := "/database/search?" + parameters

	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
