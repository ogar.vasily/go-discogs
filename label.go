package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get a label by ID
func GetLabelById(id int) (error, *models.Label) {
	data := new(models.Label)
	endpoint := fmt.Sprintf("/labels/%d", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
