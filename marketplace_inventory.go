package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"
)

// Get the list of listings in a user’s inventory
func GetInventoryByName(name string) (error, *models.Inventory) {
	data := new(models.Inventory)
	endpoint := fmt.Sprintf("/users/%s/inventory", name)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
