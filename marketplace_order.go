package go_discogs

import (
	"fmt"
	"gitlab.com/ogar.vasily/go-discogs/models"

	"net/url"
)

// Edit the data associated with an order
func EditOrderById(id string, params []byte) (error, *models.OrderEditResponse) {
	data := new(models.OrderEditResponse)
	endpoint := fmt.Sprintf("/marketplace/orders/%s", id)
	err := Post(endpoint, params, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

// Get the data associated with an order
func GetOrderById(id string) (error, *models.Order) {
	data := new(models.Order)
	endpoint := fmt.Sprintf("/marketplace/orders/%s", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

// Get list of the authenticated user’s orders.
func GetAllOrders(status, sort, sort_order, page, per_page string) (error, *models.OrderList) {
	data := new(models.OrderList)
	params := url.Values{}
	if status != "" {
		params.Add("status", status)
	}
	if sort != "" {
		params.Add("sort", sort)
	}
	if sort_order != "" {
		params.Add("sort_order", sort_order)
	}

	if page != "" {
		params.Add("page", page)
	}

	if per_page != "" {
		params.Add("per_page", per_page)
	}

	parameters := params.Encode()
	endpoint := "/marketplace/orders?" + parameters
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
