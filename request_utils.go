package go_discogs

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"gitlab.com/ogar.vasily/go-discogs/limiter"
	"fmt"
	"time"
	"io/ioutil"
	"path/filepath"
	"path"
	"os"
	"runtime"
	logrus "github.com/Sirupsen/logrus"
)

var api_url = "https://api.discogs.com"

func Get(endpoint string, data interface{}) error {
	limiter.RateLimiter.Wait()
	r, err := get(endpoint)

	if err != nil {
		return err
	}

	defer r.Body.Close()
	err = responseError(*r)
	json.NewDecoder(r.Body).Decode(&data)

	return err
}

func get(endpoint string) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("GET", url, nil)
	setHeaders(req, HeaderToken)

	return client.Do(req)
}

func Post(endpoint string, params []byte, data interface{}) error {
	limiter.RateLimiter.Wait()
	r, err := post(endpoint, params)

	if err != nil {
		return err
	}

	defer r.Body.Close()
	err = responseError(*r)
	json.NewDecoder(r.Body).Decode(&data)

	return err
}

func post(endpoint string, jsonStr []byte) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	setHeaders(req, HeaderToken)

	return client.Do(req)
}

func Delete(endpoint string) error {
	r, err := delete(endpoint)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	err = responseError(*r)

	return err
}

func delete(endpoint string) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("DELETE", url, nil)
	setHeaders(req, HeaderToken)

	return client.Do(req)
}

func responseError(r http.Response) error {


	switch r.StatusCode {
	case 200, 201, 204:
		return nil
	default:
		log := logrus.New()
		log.SetFormatter(&logrus.JSONFormatter{TimestampFormat: "02-01-2006 15:04:05", PrettyPrint: true})
		log.SetReportCaller(true)

		currentTime := time.Now()
		filename := currentTime.Format("02-01-2006") + ".log"

		_, pack, _, ok := runtime.Caller(0)
		if !ok {
			panic("No caller information")
		}

		logPath := filepath.Join(path.Dir(pack), "logs")
		os.MkdirAll(logPath, os.ModePerm)

		fullLogPath := logPath + "/" + filename
		f, err := os.OpenFile(fullLogPath, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)

		defer f.Close()
		if err != nil {
			fmt.Println(err)
		} else {
			log.SetOutput(f)
		}

		b, _ := ioutil.ReadAll(r.Body)
		errorMessage := errors.New(string(b))
		log.WithFields(logrus.Fields{"error": errorMessage}).Error("Discogs request error")
		return errorMessage
	}

}

func setHeaders(req *http.Request, token string) {
	req.Header.Set("Authorization", token)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Go-DiscogsClient/1.0")
}
